//import logo from './logo.svg';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import About from './pages/About';
import AppLayout from './pages/AppLayout';
import Books from './pages/Books';
import Home from './pages/Home';
import Login from './pages/Login';

function App() {  // app can set all in page ( home , login)
  

  // let data = [
  //   {
  //     "title": "Web 1",
  //     "subtitle": "HTML 1"
  //   },
  //   {
  //     "title": "Web 2",
  //     "subtitle": "HTML 2"
  //   },
  //   {
  //     "title": "Web 3",
  //     "subtitle": "HTML 3"
  //   },
  //   {
  //     "title": "Web 4",
  //     "subtitle": "HTML 4"
  //   },
  //   {
  //     "title": "Web 5",
  //     "subtitle": "HTML 5"
  //   },
  //   {
  //     "title": "Web 6",
  //     "subtitle": "HTML 6"
  //   },
  //   {
  //     "title": "Web 7",
  //     "subtitle": "HTML 7"
  //   }
  // ]
  return (

    <>
      {/* <AppNavbar /> */}
      <Routes>

        <Route path='/' element={<AppLayout />}>
          <Route index element={<Home />} />
          <Route path='/about-us' element={<About />} />
          <Route path='/booked' element={<Books />} />
        </Route>
        <Route path='/login' element={<Login />} />
        {/* <Route path='/' element={<Home />} />
        <Route path='/login' element={<Login />} />
        <Route path='/about-us' element={<About />} /> */}
      </Routes>
      {/* <AppFooter /> */}
    </>


    // <>
      // // {/* <Container className='mt-4'>
      //   <Row className='g-4'>

      //   {
      //   data.map(d => (
      //     <Col md ={3}>
      //       <MyCard title={d.title} subtitle={d.subtitle} />
      //     </Col>
      //   ))
      //   } */}

          // // {/* <Col md={3}>

          //   <MyCard title="Web Design" subtitle="V6"/>
            
          //   <Button variant="primary" className='mt-4'>Primary</Button>{' '}
          // </Col>
          // <Col md={3}>

          //   <MyCard title="Web Design" subtitle="V6" />

          //   <Button variant="primary">Primary</Button>{' '}
          // </Col>
          // <Col md={3}>

          //   <MyCard title="Web Design" subtitle="V6" />

          //   <Button variant="primary">Primary</Button>{' '}
          // </Col>
          // <Col md={3}>

          //   <MyCard title="App" subtitle="V9"/>

          //   <Button variant="primary">Primary</Button>{' '}
          //   <Dropdown className='mt-4'>
          //     <Dropdown.Toggle variant="success" id="dropdown-basic">
          //       Dropdown Button
          //     </Dropdown.Toggle>

          //     <Dropdown.Menu>
          //       <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
          //       <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
          //       <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
          //     </Dropdown.Menu>
          //   </Dropdown>
          // </Col>*/}
    //     {/* </Row>
    //   </Container>
    // </> */}
  )
}
export default App
