import { API_BASE_URI, AUTH_HEADER } from "./constants"

export const fetchbooks = async(pageNum, PageSize) => {
    const response = await fetch(`${API_BASE_URI}books?pageNum =${pageNum}&pageSize=${PageSize}`,{
        method: 'GET',
        headers: {
                'Authorization': AUTH_HEADER
        } 
    })
    return response.json()
}