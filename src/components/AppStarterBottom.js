import { Container } from "react-bootstrap"

const AppStarterBottom = () => {
    return (
        <Container>
            <div className="row g-5">
                <div className="col-md-6">
                    <h2>Starter projects</h2>
                    <p>Ready to beyond the starter template? Check out these open source projects that you can quickly duplicate to a new GitHub repository.</p>
                    <ul className="icon-list ps-0">
                        <li className="d-flex align-items-start mb-1"><a href="https://github.com/twbs/bootstrap-npm-starter" rel="noopener" target="_blank">Bootstrap npm starter</a></li>
                        <li className="text-muted d-flex align-items-start mb-1">Bootstrap Parcel starter (coming soon!)</li>
                    </ul>
                </div>

                <div className="col-md-6">
                    <h2>Guides</h2>
                    <p>Read more detailed instructions and documentation on using or contributing to Bootstrap.</p>
                    <ul className="icon-list ps-0">
                        <li className="d-flex align-items-start mb-1"><a href="/docs/5.3/getting-started/introduction/">Bootstrap quick start guide</a></li>
                        <li className="d-flex align-items-start mb-1"><a href="/docs/5.3/getting-started/webpack/">Bootstrap Webpack guide</a></li>
                        <li className="d-flex align-items-start mb-1"><a href="/docs/5.3/getting-started/parcel/">Bootstrap Parcel guide</a></li>
                        <li className="d-flex align-items-start mb-1"><a href="/docs/5.3/getting-started/vite/">Bootstrap Vite guide</a></li>
                        <li className="d-flex align-items-start mb-1"><a href="/docs/5.3/getting-started/contribute/">Contributing to Bootstrap</a></li>
                    </ul>
                </div>
            </div>
        </Container>
    )
}
export default AppStarterBottom