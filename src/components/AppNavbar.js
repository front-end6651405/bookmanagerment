import { Container, Image, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { Link } from "react-router-dom"
import LogoBrand from '../Assets/Img/logo.png';
import '../Assets/css/AppNavbar.css';

const AppNavbar = () => {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="mb-4">
            <Container>
                <Link className="nav-link" to={'/'}>
                    <Image className="brand-logo" src={LogoBrand} />
                </Link>
                <Navbar.Brand href="#home">ISTAD Reading</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        {/* <Nav.Link href="#features">Home</Nav.Link> */}
                        <Link className="nav-link" to={'/'}>Home</Link>
                        <Link  className="nav-link" to={'/booked'}>Books</Link>
                        <NavDropdown title="Categories" id="collasible-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Popular</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">
                                Sceince & Fiction
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Technology</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">
                                More
                            </NavDropdown.Item>
                        </NavDropdown>
                        <Link className="nav-link" to={'/about-us'}>About Us</Link>
                    </Nav>
                    <Nav>
                        {/* <Nav.Link href="#deets">Login</Nav.Link> */}
                        <Link className="nav-link" to={'/login'}>Login</Link>
                        <Nav.Link eventKey={2} href="#memes">
                            Register
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )


}
export default AppNavbar