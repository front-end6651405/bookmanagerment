import { Button, Card } from "react-bootstrap"
// import Book1 from '../Assets/Img/Book1.jpg'

const BookCard = (props) => {
    return (
        <Card>
            {/* <Card.Img variant="top" src="https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/207075009/original/ba5f73d4e2f2d1fc5f312b7968da78f84056c546/design-minimal-unique-book-cover.jpg" /> */}
            <Card.Img></Card.Img>
            <Card.Body>
                <Card.Img variant="top" src = {props.book} />
                <Card.Text>
                    Some quick example text to build on the card title and make up the
                    bulk of the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
        </Card>
    )
}
export default BookCard