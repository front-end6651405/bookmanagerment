import { Col, Container, Row } from 'react-bootstrap';
import AppFeatures from '../components/AppFeatures';
import AppStarter from '../components/AppStarter';
import BookCard from '../components/BookCard';
import Book1Img from '../Assets/Img/Book1.jpg';
import Book2Img from '../Assets/Img/Book2.jpg';
function Home() {
    return (
        <>
            
            <AppFeatures />
            <AppStarter />



            {/* List 3 cards in a row */}
            <Container>
                <h1 className='my-4'>Find your fav books here</h1>
                <Row className='g-3'>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book = {Book1Img}/>
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book1Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book1Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book2Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book2Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book2Img} />
                    </Col>
                </Row>
            </Container>

            
        </>
    )

}
export default Home;