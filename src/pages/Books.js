
import { useEffect, useState } from "react"
import { Col, Container, Row } from "react-bootstrap"
import BoookPagination from "../components/BookPagination"
import BookStory from "../components/BookStory"
import { fetchbooks } from "../service/BookService"

const Books = () => {
    const [books, setBooks] = useState ([{}])
    const onBookFetched = (pageNum, pageSize) => {
        fetchbooks(pageNum, pageSize)
        .then(json => {
            //handle UI
            console.log(json)
            //update state
            setBooks(json.data.list)
        })
    }

    useEffect (() => {
        onBookFetched(1, 10)
    } ,[])



        return (
            <>
                <Container className="mt-4">
                        <Row>
                            {
                                books && books.map((book, index) => (
                                    <Col key={index} md={6}>
                                        <BookStory book={book}/>
                                    </Col>
                                ))
                            }
                        </Row>
                        <BoookPagination />
                </Container>
            </>
    )
}
export default Books